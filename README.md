# Family Tree (CSCI 30 Finals Project)

### Team members: Anton Benitez, Tomas Falgui, Cian Laguesma

## File List
- `cli.py`
- `file_manager.py`
- `node.py`
- `Cert_of_Authorship_Group.pdf`
- `README.md`
- `Contributions.pdf`

## Minimum machine specs
- Machine must have at least a memory of 2GB
- Machine must contain at least python 3.7 version

## Special cases
- Our program also handles if the number of links specified exceeds the actual number of links made.
- Our program handles jumbled child links wherein child and grandchildren nodes are linked before being linked to the parent. 
> Child B C

> Child A B

> Child Z A

- Our list of commands include ‘help’ which brings up the list of commands again.
- We utilized gitlab in making our project and followed gitflow to ensure an organized flow.
- The program also has a test_node file for unit testing. It tests the class Node and the functions: in_order, pre_order, post_order, tree_printing, count_nodes, and node_insertion


## Running Instructions
1. Go to your terminal or the command prompt
2. Change your current working directory to this project's directory
3. Run `py cli.py` or `python cli.py` or `python3 cli.py`.