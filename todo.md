# Error Handling

## Loading a family tree from a file

- [x] Error: file does not exist
- [x] Error: unrecognized keyword [kw]
- [x] Error: person [name] already exists
- [ ] Error: person details are insufficient
- [x] Error: declared count does not match number of person entries
- [x] Error: attempted to link nonexistent person(s)
- [ ] Error: family tree is ill-formed; check if cycles are present
- [x] Error: family tree is disconnected; check for orphans
- [x] Error: cannot set root node of family as a child.

## Querying information about a family tree

- [x] Error: a family tree must be loaded in the current session first
- [x] Error: no such person [name]
- [x] Error: inputs must be distinct

## Edit a person's entry in the family tree

- [ ] Error: name [new-name] already in use
