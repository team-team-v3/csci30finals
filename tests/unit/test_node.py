import unittest
from node import Node, in_order, pre_order, post_order, tree_printing, count_nodes, node_insertion


class TestNode(unittest.TestCase):
    def setUp(self):
        self.root = Node("Anton", 20, True)
        node_insertion(self.root, "Anton", "Tomy", 19, True)
        node_insertion(self.root, "Tomy", "Cian", 18, True)

    def test_add_children(self):
        self.assertEqual(self.root.left.name, "Tomy")
        self.assertEqual(self.root.left.left.name, "Cian")

    def test_in_order(self):
        self.assertEqual(in_order(self.root), ['Cian', 'Tomy', 'Anton'])
        in_order(self.root)

    def test_pre_order(self):
        self.assertEqual(pre_order(self.root), ['Anton', 'Tomy', 'Cian'])
        pre_order(self.root)

    def test_post_order(self):
        self.assertEqual(post_order(self.root), ['Cian', 'Tomy', 'Anton'])
        post_order(self.root)

    def test_tree_printing(self):
        self.assertEqual(tree_printing(self.root), [
                         'Name: Cian Age: 18 Sex: True ', 'Name: Tomy Age: 19 Sex: True ', 'Name: Anton Age: 20 Sex: True '])

    def test_node_counting(self):
        self.assertEqual(count_nodes(self.root), 3)

    def test_insert_nodes(self):
        node_insertion(self.root, "Anton", "Brian", 22, True)
        self.assertEqual(self.root.right.name, 'Brian')


if __name__ == '__main__':
    unittest.main()
