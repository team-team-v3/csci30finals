import os
from node import node_insertion, Node, tree_printing, count_nodes, clear, name_check, pre_order, node_value_return, node_value_return_values, hasCycle

#File_manager.py handles the files and its data
class CustomError(Exception):
    def __init__(self, message):

        # Call the base class constructor with the parameters it needs
        super().__init__(message)


#returns the chunks of data
def chunks(lst, n):
    return [lst[i:i + n] for i in range(0, len(lst), n)]


#reads the file input
def read_file(file_name):
    fileDir = os.path.dirname(os.path.realpath('__file__'))
    filename = os.path.join(fileDir, file_name)
    contents = ""

    if os.stat(file_name).st_size == 0:
        raise CustomError("Error: file is empty.")

    with open(filename) as f:
        for line in f.readlines():
            contents += line

    return contents

#processes the file's content
def process_file(contents):
    keywords = ['family', 'count', 'name', 'age', 'sex', 'links', 'child']
    meta_data = contents.split('\n')[0:2]
    fam_name = meta_data[0].split(' ')
    fam_count = meta_data[1].split(' ')

    if not fam_name[0] in keywords:
        raise CustomError('Error: unrecognized keyword {}'.format(fam_name[0]))
    if not fam_count[0] in keywords:
        raise CustomError(
            'Error: unrecognized keyword {}'.format(fam_count[0]))
    fam_name = fam_name[1]
    fam_count = int(fam_count[1])
    members = contents.split('\n')[2:]
    links = members[3*fam_count:]
    link_value = links[0].split(' ')
    link_child = links[1:]
    chunked_members = chunks(members[:3*fam_count], 3)
    nameCount = 0
    ageCount = 0
    sexCount = 0
    data = []

    for mem in chunked_members:
        name = mem[0].split(' ')
        if 'name' in name:
            nameCount += 1
        if not name[0] in keywords:
            raise CustomError('Error: unrecognized keyword {}'.format(name[0]))

        age = mem[1].split(' ')
        if 'age' in age:
            ageCount += 1
        if not age[0] in keywords:
            raise CustomError('Error: unrecognized keyword {}'.format(age[0]))

        sex = mem[2].split(' ')
        if 'sex' in sex:
            sexCount += 1
        if not sex[0] in keywords:
            raise CustomError('Error: unrecognized keyword {}'.format(sex[0]))
        if (nameCount != ageCount or nameCount != sexCount or ageCount != sexCount) or (nameCount == 0 or ageCount == 0 or sexCount == 0):
            raise CustomError('Error: person details are insufficient')
        sex = True if sex[1] == 'true' else False

        for x in data:
            if x['name'] == name[1]:
                raise CustomError(
                    'Error: person {} already exists'.format(name[1]))

        data.append({'name': name[1], 'age': age[1], 'sex': sex})

    root = Node(data[0]['name'], data[0]['age'], data[0]['sex'])
    rootChildChecker = root.name
    if not link_value[0] in keywords:
        raise CustomError(
            'Error: unrecognized keyword {}'.format(link_value[0]))
    try:
        link_value = int(link_value[1])
    except ValueError:
        raise CustomError(
            'Error: declared count does not match number of person entries')

    init_array = []
    x = 0
    while x < link_value:
        try:
            init_array.append(link_child[x].split(' '))
        except IndexError:
            raise CustomError(
                'Error: family tree is disconnected; check for orphans')
        x += 1

#links the nodes together
    def node_linking(root):
        to_link = []
        y = 0
        while y < len(init_array):
            try:
                if init_array[y][1] == root.name:
                    to_link.append(init_array[y])
                if init_array[y][2] == rootChildChecker:
                    raise CustomError(
                        'Error: cannot set root node of family as a child')
                y += 1
            except IndexError:
                raise CustomError(
                    'Error: declared links does not match number of links created')

        child_obj = None
        parent_obj = None
        z = 0
        while z < len(to_link):
            for a in data:
                if a['name'] == to_link[z][1]:
                    parent_obj = a
                elif a['name'] == to_link[z][2]:
                    child_obj = a

            if child_obj is None or parent_obj is None:
                raise CustomError(
                    'Error: attempted to link nonexistent person(s)')

            node_insertion(
                root, parent_obj['name'], child_obj['name'], child_obj['age'], child_obj['sex'])

            # loop to check array if child is a parent to make recursive calls and inserts the nodes
            b = 0
            while b < len(init_array):
                # Check if the child is a parent to make recursive calls
                if to_link[z][2] == init_array[b][1]:
                    # go to wherever the child is if its on the left or right
                    if root.left is not None and root.right is None:
                        node_linking(root.left)
                    elif root.left is not None and root.right is not None:
                        node_linking(root.right)
                b += 1
            z += 1

    node_linking(root)
    to_check = [x['name'] for x in data]
    if (hasCycle(root)):
        raise CustomError(
            'Error: family tree is ill-formed; check if cycles are present')

    if count_nodes(root) != len(to_check):
        raise CustomError(
            'Error: family tree is disconnected; check for orphans')
    return [root, fam_name, to_check]

#loads the file inputted
def load_file(file_name):
    contents = read_file(file_name)
    return process_file(contents)


#saves the file with the desired name
def save_file(root, file_name, fam_name, order_of_nodes):
    count = count_nodes(root)
    for a in order_of_nodes:
        node_value_return(root, a)
    links = []
    with open(file_name, 'w') as f:
        f.write('family {}\n'.format(fam_name))
        f.write('count {}\n'.format(str(count)))
        for val in node_value_return_values:
            f.write('name {}\n'.format(val[0]))
            f.write('age {}\n'.format(val[1]))
            f.write('sex {}\n'.format('true' if val[2] else 'false'))
            if not 'None' in val[3]:
                links.append(val[3])
            if not 'None' in val[4]:
                links.append(val[4])
        f.write('links {}\n'.format(count - 1))
        for link in links:
            f.write('{}\n'.format(link))
    clear()
    print("Saved {} family tree in session to {}".format(fam_name, file_name))
