from file_manager import load_file, save_file, CustomError
from node import node_value, ancestors, descendants, clear, node_modifier, count_nodes, relationship, name_check

#cli.py handles the list of commands
def main():
    print('\n-------------------------')
    print('Welcome to Family Tree Program. Please load a family tree.')
    print('\nList of commands: \ncount\nage [name]\nsex [name]\nancestors [name]\ndescendants [name]\nedit-name [name] [new-name]\nedit-age [name] [new-age]\nedit-sex [name] [new-sex]\nrelationship [name1] [name2]\nload [filename]\nsave [filename]\nhelp\nbye')
    root = None
    family_name = None
    order_of_nodes = []
    commands = ['count', 'age', 'sex', 'ancestors', 'descendants', 'edit-name',
                'edit-age', 'edit-sex', 'relationship', 'load', 'save', 'help', 'bye']
    while True:
        command = input().rstrip().split(' ')
        if command[0] == '' or len(command) > 3:
            print('Invalid number of inputs')
            continue

        if command[0] == 'bye':
            print("Goodbye!")
            break
        #brings up the list of commands
        elif command[0] == 'help':
            print('\nList of commands: \ncount\nage[name]\nsex[name]\nancestors[name]\ndescendants[name]\nedit-name[name][new-name]\nedit-age[name][new-age]\nedit-sex[name][new-sex]\nrelationship[name1][name2]\nload[filename]\nsave[filename]\nhelp\nbye')
        # count
        elif command[0] == 'count' and root is not None:
            print(count_nodes(root))
            clear()
        # query
        elif command[0] == 'age' and root is not None:
            if len(command) < 2:
                print('Invalid number of inputs')
                continue
            name = command[1]
            if not name_check(root, name):
                print('Error: no such person {}'.format(name))
                continue
            node_value(root, name, True)
        # query
        elif command[0] == 'sex' and root is not None:
            if len(command) < 2:
                print('Invalid number of inputs')
                continue
            name = command[1]
            if not name_check(root, name):
                print('Error: no such person {}'.format(name))
                continue
            node_value(root, name, False)
        # query
        elif command[0] == 'ancestors' and root is not None:
            if len(command) < 2:
                print('Invalid number of inputs')
                continue
            name = command[1]
            if not name_check(root, name):
                print('Error: no such person {}'.format(name))
                continue
            print(*ancestors(root, name), sep=" ")
            clear()
        # query
        elif command[0] == 'descendants' and root is not None:
            if len(command) < 2:
                print('Invalid number of inputs')
                continue
            name = command[1]
            if not name_check(root, name):
                print('Error: no such person {}'.format(name))
                continue
            print(*descendants(root, name), sep=" ")
            clear()
        # update
        elif command[0] == 'edit-name' and root is not None:
            if len(command) < 3:
                print('Invalid number of inputs')
                continue
            old_name = command[1]
            if not name_check(root, old_name):
                print('Error: no such person {}'.format(old_name))
                continue
            new_name = command[2]
            if name_check(root, new_name):
                print("Error: name " + new_name + " already in use")
                continue
            order_of_nodes[order_of_nodes.index(old_name)] = new_name
            node_modifier(root, old_name, new_name, None, None, 'Name')
        # update
        elif command[0] == 'edit-age' and root is not None:
            if len(command) < 3:
                print('Invalid number of inputs')
                continue
            name = command[1]
            if not name_check(root, name):
                print('Error: no such person {}'.format(old_name))
                continue
            new_age = command[2]
            try:
                new_age = int(new_age)
                if new_age < 0:
                    print("Provide a valid number.")
                    continue
            except ValueError:
                print("Provide a valid number.")
                continue
            
            new_age = str(new_age)

            node_modifier(root, name, None, new_age, None, 'Age')
        # update
        elif command[0] == 'edit-sex' and root is not None:
            if len(command) < 3:
                print('Invalid number of inputs')
                continue
            name = command[1]
            if not name_check(root, name):
                print('Error: no such person {}'.format(old_name))
                continue
            new_sex = command[2]
            if new_sex == "true":
                new_sex = True
            else:
                new_sex = False
            node_modifier(root, name, None, None, new_sex, 'Sex')
        # query
        elif command[0] == 'relationship' and root is not None:
            if len(command) < 3:
                print('Invalid number of inputs')
                continue
            name_1 = command[1]
            name_2 = command[2]
            name1_exists = name_check(root, name_1)
            name2_exists = name_check(root, name_2)

            if not name1_exists:
                print('Error: no such person {}'.format(name_1))
                continue
            elif not name2_exists:
                print('Error: no such person {}'.format(name_2))
                continue
            elif not name1_exists and not name2_exists:
                print('Error: no such people: {} & {}'.format(name_1, name_2))
                continue

            if name_1 == name_2:
                print('Error: inputs must be distinct')

            print(relationship(root, name_1, name_2))
        #load
        elif command[0] == 'load':
            file_name = command[1]
            try:
                data = load_file(file_name)
            except FileNotFoundError:
                print('Error: file does not exist')
                continue
            except CustomError as e:
                print(e)
                clear()
                continue
            root = data[0]
            family_name = data[1]
            order_of_nodes = data[2]
            print("Successfully loaded {}".format(file_name))
            clear()
        # save
        elif command[0] == 'save' and root is not None:
            file_name = command[1]
            save_file(root, file_name, family_name, order_of_nodes)
        elif root is not None:
            if not command in commands:
                print("Invalid Command")
                print(
                    '\nList of commands: \ncount\nage[name]\nsex[name]\nancestors[name]\ndescendants[name]\nedit-name[name][new-name]\nedit-age[name][new-age]\nedit-sex[name][new-sex]\nrelationship[name1][name2]\nload[filename]\nsave[filename]\nhelp\nbye')
        else:
            print('Error: a family tree must be loaded in the current session first')
            continue


if __name__ == '__main__':
    main()
