#Family tree class
class Node:
    def __init__(self,name,age,sex):
        self.left = None
        self.right = None
        self.name = name
        self.age = age
        self.sex = sex

    def reset(self):
        self.left = None
        self.right = None
        self.name = None
        self.age = None 
        self.sex = None
      
#Recursive in-order tree traversal function
in_order_list = []
def in_order(root):
    if root:
        in_order(root.left)
        in_order_list.append(root.name)
        in_order (root.right)
    return in_order_list

#Recursive pre-order tree traversal function
pre_order_list = []
def pre_order(root):
    if root:
        pre_order_list.append(root.name)
        pre_order(root.left)
        pre_order(root.right)
    return pre_order_list

#Recursive post-order tree traversal function
post_order_list = []
def post_order(root):
    if root:
        post_order(root.left)
        post_order(root.right)
        post_order_list.append(root.name)
    return post_order_list

#Determine number of nodes in a tree
node_count = 0
def count_nodes(root):
    global node_count
    if root:
        count_nodes(root.left)
        count_nodes(root.right)
        node_count += 1
    return node_count


#Printing the binary tree
unittest_tree_print = []
def tree_printing(root):
    global unittest_tree_print
    if root is None:
        return None
    if root.left:
        tree_printing(root.left)
    print("Name: " + root.name)
    print("Age: " + str(root.age))
    print("Sex: " + str(root.sex))
    unittest_tree_print.append("Name: " + root.name + " Age: " + str(root.age) + " Sex: " + str(root.sex) + " ")
    if root.right:
        tree_printing(root.right)
    return unittest_tree_print   
  
#Returns the root of the least common ancestor
def LCA(root, p1, p2):
    if root is None:
        return None
    if (root.name == p1.name or root.name == p2.name):
        return root
    left = LCA(root.left, p1, p2)
    right = LCA(root.right, p1, p2)
    if left is not None and right is not None:
        return root
    if left:
        return left
    else:
        return right

#Returns the root of the least common ancestor
def LCA_Return_Root(root, p1, p2):
    if root is None:
        return None
    if (root.age > p1 and root.age > p2):
        
        return LCA_Return_Root(root.left, p1, p2)
    if (root.age < p1 and root.age < p2):
        return LCA_Return_Root(root.right, p1, p2)
    return root

#Finds the distance of the two nodes
def findDistance(root,p1,p2):
    if root:
        path1 = []
        getPath(root, path1, p1)
        path2 = []
        getPath(root, path2, p2)
        i = 0
        while i < len(path1) and i < len(path2):
            if path1[i] != path2[i]:
                break
            i += 1
        return (len(path1)+len(path2)-2*i)
    else:
        return 0
    
#Finds the path 
def getPath(root,path,p):
    if (root is None):
        return False
    path.append(root.name)
    if (root.name == p.name):
        return True
    if ((root.left != None and getPath(root.left, path, p)) or  (root.right != None and getPath(root.right, path, p))):
        return True
    path.pop()
    return False
    
#Returns the Root of the age p when searching for the name p
def searchName(root,p):
    if root == None:
        return False
    if root.name == p:
        return root
    res1 = searchName(root.left,p)
    res2 = searchName(root.right,p)  
    return res1 or res2 

#Function to check if there's a similar name already
def name_check(root, passed_name):
    clear()
    pre_order(root)
    for elements in pre_order_list:
        if elements == passed_name:
            clear()
            return True
    clear()
    return False

#Returns ordinal
def Ordinal(number):
    ordinals = ["first","second","third","fourth","fifth","sixth","seventh","eighth","ninth","tenth"]
    nums = [x for x in range (1, 11)]
    mapping = dict(zip(nums, ordinals))
    return mapping[number]

#Returns number of times removed
def Times(number):
    timesRemoved = ["once", "twice", "thrice", "four times", "five times", "six times", "seven times", "eight times", "nine times", "ten times"]
    nums = [x for x in range (1, 11)]
    mapping = dict(zip(nums, timesRemoved))
    return mapping[number]

#Returns number of 'great'
def Great(number):
    greatTimes = ["", "great", "great-great", "great-great-great-", "great-great-great-great-", "great-great-great-great-great-", "great-great-great-great-great-great-", "great-great-great-great-great-great-great-", "great-great-great-great-great-great-great-great-", "great-great-great-great-great-great-great-great-great-"]
    nums = [x for x in range (1, 11)]
    mapping = dict(zip(nums, greatTimes))
    return mapping[number]

#Returns term 
#d1 and d2 are distances of p1 and p2 from the root  
def Term(kind,d1,d2,sex):
    if kind == 'child':
        if d1 < d2:
            if sex == True:
                return "son"
            else:
                return "daughter"
        else:
            if sex == True:
                return "father"
            else:
                return "mother"
    elif kind == 'nibling':
        if d1 < d2:
            if sex == True:
                return "nephew"
            else:
                return "niece"
        else:
            if sex == True:
                return "uncle"
            else:
                return "aunt"
            
#Determines the relationship between two distinct people in the family tree, from the perspective of p1
#p1 will be a node p2 will be a node
def relationship(root,p1,p2):
    if p1 != p2:
        p1 = searchName(root,p1) #Returns root of p1 and p2
        p2 = searchName(root,p2)
        l = LCA(root,p1,p2)
        d1 = findDistance(root,p1,l)
        d2 = findDistance(root,p2,l)
        nearest = min(d1,d2)
        furthest = max(d1,d2)
        if nearest >= 2:
            degree = nearest - 1
            removal = abs(d1-d2)
            if removal == 0:
                return (Ordinal(degree) + " cousin")
            else:
                return (Ordinal(degree)+ " cousin" + Times(removal) + " removed" )
        elif nearest == 1:
            if furthest == 1:
                if p2.sex == True:
                    return "brother"
                else:
                    return "sister"
            elif furthest == 2:
                return (Term("nibling", d1 , d2, p2.sex))
            else:
                return (Great(furthest-2)+ "grand" + Term("nibling",d1,d2,p2.sex))
        elif nearest == 0:
            if furthest == 1:
                return (Term("child", d1, d2, p2.sex))
            else:
                return (Great(furthest-1) + "grand" + Term("child",d1,d2,p2.sex))
    else:
        return ("Error: inputs must be distinct")
            
#Function for node insertion
def node_insertion(root, parent, name, age, sex):
    if root is None and parent is None:
        print("No Root")
        root = Node(name,age,sex)
    elif root:
        if root.name == parent:
            if root.left is None:
                root.left = Node(name,age,sex)
            elif root.left is not None:
                root.right = Node(name,age,sex)
        else: #put recursive calls in an else function to stop calling the function once the node has been added
            node_insertion(root.left, parent, name, age, sex)
            node_insertion(root.right, parent, name, age, sex)

        
#Function for editing nodes
def node_modifier(root, name, new_name, new_age, new_sex, change): 
    if root and name_check(root, name) is True:
        if change == "Name" and name_check(root, new_name) is False:
            inner_name_modifier(root, name, new_name)
        elif change == "Name" and name_check(root, new_name) is True:
            pass
        elif change == "Age":
            inner_age_modifier(root, name, new_age)
        elif change == "Sex":
            inner_sex_modifier(root, name, new_sex)

#Function for changing node name
def inner_name_modifier(root, name, new_name):
    if root is None:
        return None
    if root.name == name:
        old_name = root.name
        root.name = new_name
        return print(old_name + "'s new name is " + root.name)
    inner_name_modifier(root.left, name, new_name)
    inner_name_modifier(root.right, name, new_name)
    
#Function for changing node age
def inner_age_modifier(root, name, new_age):
    if root is None:
        return None
    if root.name == name:
        root.age = new_age  
        return print(root.name + "'s new age is " + str(root.age))
    else:
        inner_age_modifier(root.left, name, new_age)
        inner_age_modifier(root.right, name, new_age)

#Function for changing node sex
def inner_sex_modifier(root, name, new_sex):
    if root is None:
        return None
    if root.name == name:
        root.sex = new_sex
        sex_str = ""
        if root.sex:
            sex_str = "Male"
        else:
            sex_str = "Female"
        return print(root.name + "'s new sex is " + sex_str)
    else:
        inner_sex_modifier(root.left, name, bool(new_sex))
        inner_sex_modifier(root.right, name, bool(new_sex))
         
#Returns age or sex of the node given its name
def node_value(root, name, isAge):
    if root:
        if root.name == name:
            if isAge:
                return print(root.name + "'s age is " + str(root.age))
            else:
                if root.sex:
                    return print(root.name + "'s sex is male")
                else:
                    return print(root.name + "'s sex is female")
        node_value(root.left, name, isAge)
        node_value(root.right, name, isAge)

#Returns data values of a node
node_value_return_values = []
def node_value_return(root, name):
    if root:
        if root.name == name:
            global node_value_return_values
            root_node = root.name
            root_left_name = root.left.name if root.left is not None else None
            root_right_name = root.right.name if root.right is not None else None

            node_value_return_values.append((root.name, root.age, root.sex, 'child {} {}'.format(root_node, root_left_name), 'child {} {}'.format(root_node, root_right_name)))
        else:
            node_value_return(root.left, name)
            node_value_return(root.right, name)

    # return node_value_return_values

#Returns ancestors of p
ancestorList = []
def ancestors(root,p):
    global ancestorList
    if root == None:
        return False
    if root.name == p:
        return ['No','Ancestors']
    if (ancestors(root.left,p) or ancestors(root.right,p)):
        ancestorList.append(root.name)
        ancestorList.sort()
        return ancestorList
    return False

#Returns descendants of p
def descendants(root,p):
    p = searchName(root,p)
    descendantList = pre_order(p)
    if len(descendantList)==1:
        return ['No','Descendants']
    else:
        descendantList.pop(0)
        descendantList.sort()
        return descendantList
    clear()

 
#Function for clearing the values stored in the following lists and variables
def clear():
    global ancestorList
    ancestorList.clear()
    global in_order_list
    in_order_list.clear()
    global pre_order_list 
    pre_order_list.clear()
    global post_order_list 
    post_order_list.clear()
    global node_count
    node_count = 0
    global unittest_tree_print 
    unittest_tree_print.clear()
    global node_value_return_values
    node_value_return_values.clear()

def hasCycle(root):
    array = pre_order(root)
    if len(array) != len(set(array)):
        return True
    else:
        return False
    clear()